var indexSectionsWithContent =
{
  0: "bcdgiklmoprst",
  1: "bt",
  2: "bco",
  3: "bcgmps",
  4: "cdiklprt",
  5: "bt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs"
};

var indexSectionLabels =
{
  0: "Tutto",
  1: "Classi",
  2: "File",
  3: "Funzioni",
  4: "Variabili",
  5: "Ridefinizioni di tipo (typedef)"
};


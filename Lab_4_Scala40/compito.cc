#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;

#include "tipo.h"
#include "liste.h"

/*
 * pesca: aggiunge alla lista ordinata delle carte in mano ad un giocatore una nuova carta.
 */
void pesca(lista& l){
	tipo_inf carta;

	cout<<"Seme (C = cuori, F = fiori, P = picche, Q = quadri): ";
	cin>>carta.seme;

	cout<<"Inserire valore della carta: ";
	cin>>carta.valore;

	elem* e1 = new_elem(carta);
	l = ord_insert_elem(l, e1);


}

void stampa(lista l, ofstream& o){
	while(l != NULL){
		print(head(l),o);
		l = tail(l);
	}
	cout<<endl;
}

/*
 * sequenza di almeno 3 carte dello stesso seme e consecutive se esiste.
 * NULL altrimenti
 */
void scala(lista carte, int& lunghezza){
	lista scala = NULL;
	lista fine = NULL;

	while(carte != NULL){
		if(head(carte).seme == head(tail(carte)).seme){
			scala = ord_insert_elem(scala, carte);
			fine = tail(carte);
			lunghezza++;
		}
		else{
			scala = ord_insert_elem(scala, fine);
			lunghezza++;
		}
		carte = tail(carte);
	}

	while(scala != NULL){
		cout<<scala->inf.seme<<" "<<scala->inf.valore<<" | ";
		scala = tail(scala);
	}
} // da finire

int main(){

	lista giocatore1 = NULL;
	lista giocatore2 = NULL;

	int n;
	cout<<"Quante carte distribuire? ";
	cin>>n;

	for(int i = 0; i < n; i++){
		cout<<"--- Carta "<<i+1<<" giocatore 1 ---"<<endl;
		pesca(giocatore1);
		cout<<endl;

		cout<<"--- Carta "<<i+1<<" giocatore 2 ---"<<endl;
		pesca(giocatore2);
		cout<<endl;
	}

	ofstream o("output");

	o<<"Le carte del giocatore 1 sono: ";
	stampa(giocatore1,o);
	o<<endl;
	o<<"Le carte del giocatore 2 sono: ";
	stampa(giocatore2,o);

	cout<<"Success!"<<endl;
	return 0;
}



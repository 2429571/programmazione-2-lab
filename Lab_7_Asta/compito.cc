#include <iostream>
#include <fstream>
using namespace std;

#include "asta.h"
#include "grafo.h"

void add(graph& g, int u, int v, float w, bool o){
    if(o)
        add_arc(g, u, v, w);
    else
        add_edge(g, u, v, w);
}

void stampaGrafo(graph G){
    adj_list temp;
    for(int i = 1; i <= get_dim(G); i++){
        temp = get_adjlist(G,i);
        cout<<i;
        while(temp){
            cout<<" -->"<<get_adjnode(temp)<<" "<<get_adjweight(temp)<<" ";
            temp = get_nextadj(temp);
        }
        cout<<endl;
    }
}

int main(){
	int n1, n2, N;
	ifstream a;
	a.open("asta");

	a>>n1>>n2>>N;

	tipo_inf* asta = new tipo_inf[N];
	for(int i = 1; i <= N; i++){
		a>>asta->tipo>>asta->valore;
	}

	int v1, v2, w;
	graph G = new_graph(N);

	while (a>>v1>>v2>>w){
	   add(G, v1, v2, w, 1);
	}

    cout<<"Il grafo ha dimensione: "<<get_dim(G)<<endl;
    stampaGrafo(G);
	return 0;
}

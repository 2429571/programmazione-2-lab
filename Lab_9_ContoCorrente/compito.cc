/** ************************************************************************************
 * @mainpage
 * Realizzare un programma per la gestione di un conto corrente.
 * Per ogni operazione registrata nel conto, viene mantenuta la data (stringa nella forma aammgg),
 * la cifra (float), un booleano che indica se l’operazione è a debito o a credito
 * (true se a debito, false altrimenti) e un carattere che ne identifica il tipo
 * (B – Bonifico, U – utenza, C – carta).
 * Le operazioni vengono mantenute in un binary search tree (BST)
 * al fine di velocizzare la ricerca per data (che è quindi la chiave).
 * Per eseguire il mio progetto da terminale:
 * entrare nella cartella: cd compito_119901 - compilare il progetto attraverso il comando: make -f Makefile -
 * eseguire il progetto attraverso il comando: make -f Makefile run
 * *********************************************************************************** */

#include <iostream>
#include <fstream>
using namespace std;

#include "op.h"
#include "bst.h"

/// Funzione che carica il bst a partire da un file di testo

/** ********************************************************************************
 * Estrae i dati dal file e crea un bst con chiave data, un array di 6 caratteri.
 *
 * @return bst
 ******************************************************************************** */

bst crea(){
	ifstream f;
	f.open("conto.txt");

	int n, deb_cred;
	f>>n;

	tipo_inf conto;
	tipo_key data;
	bst banca = NULL;

	for(int i = 0; i < n; i++){
		f.ignore();
		f.get(data, 7);

		f>>conto.cifra;
		f>>deb_cred;
		f>>conto.tipo;

		if(deb_cred == 0)
			conto.deb_cred = false;
		else
			conto.deb_cred = true;

		bnode* e = bst_newNode(data, conto);
		bst_insert(banca, e);
	}

	f.close();
	return banca;
}

/// Funzione che stampa il bst appena caricato.

/** ********************************************************************************
 * Attraverso l'esporazione DFS viene stampato l'albero in ordine crescente di data
 * dell'operazione.
 * @param bst
 ******************************************************************************** */
void stampa(bst node){
	if (get_left(node) != NULL)
		stampa(get_left(node));

	cout<<node->key<<" - ";
	print(node->inf);

	if (get_right(node) != NULL)
		stampa(get_right(node));
}

/// Funzione che stampa il saldo attuale del conto in esame.

/** ********************************************************************************
 *	Funzione ricorsiva che calcola il saldo del conto corrente.
 * @param bst
 * @return s il saldo del conto corrente.
 ******************************************************************************** */
float saldo(bst b){
	float s = 0;

	if(b == NULL)
		return 0;

	if(b->inf.deb_cred == true)
		s = s - b->inf.cifra;
	else
		s = s + b->inf.cifra;

	if(b->left != NULL)
		s = s + saldo(b->left);
	if(b->right != NULL)
		s = s + saldo(b->right);

	return s;
}

int main(){
	bst banca = crea();

	cout<<endl<<"--- stampa in ordine crescente ---"<<endl;
	stampa(banca);


	cout<<endl<<"Il saldo attuale: "<<saldo(banca)<<endl;
	return 0;
}

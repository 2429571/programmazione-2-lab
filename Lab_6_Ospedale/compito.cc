#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;

#include "grafo.h"
#include "ospedale.h"

void add(graph& g, int u, int v, float w, bool o){
    if(o)
        add_arc(g, u, v, w);
    else
        add_edge(g, u, v, w);
}

graph graphBuild(ifstream& g, bool orientato, bool pesato){
    int n, arc;
    g>>n>>arc;
    graph G = new_graph(n);
    int v1,v2;

    char* line = new char[19];
    for(int i = 0; i <= n; i++){
    	g.getline(line,20);
    }

    if(pesato){
        float w;
        while(g>>v1>>v2>>w)
            add(G,v1,v2,w,orientato);
    }
    else {
        float w;
        while (g >> v1 >> v2)
            add(G, v1, v2,1.0, orientato);
    }

    return G;
}

void stampaGrafo(graph G){
    adj_list temp;
    for(int i = 1; i <= get_dim(G); i++){
        temp = get_adjlist(G,i);
        cout<<i;
        while(temp){
            cout<<"-->"<<get_adjnode(temp)<<" ";
            temp = get_nextadj(temp);
        }
        cout<<endl;
    }
    cout<<endl;

}

void stampaRelazioni(graph G, tipo_inf* h){
	adj_list temp;

	for(int i = 1; i <= get_dim(G); i++){
		temp = get_adjlist(G,i);
	    while(temp){
	    	cout<<i;
	    	if(h[i].cittadino_medico == 'M' && h[temp->node+1].cittadino_medico == 'C')
	    		cout<<" VISITA "<<get_adjnode(temp)<<endl;
	    	if(h[i].cittadino_medico == 'C' && h[temp->node+1].cittadino_medico == 'C')
	    		cout<<" IN CAMERA "<<get_adjnode(temp)<<endl;
	        temp = get_nextadj(temp);
	    }
	}
}

int main(int argc, char* argv[]) {
	/* PUNTO 1 */
    if(argc<3) {
        cout << "Gli argomenti inseriti non sono sufficienti!" << endl;
        exit(0);
    }

    ifstream g;
    g.open(argv[1]);
    int orientato = atoi(argv[2]);		// 1 = vero 	0 = falso
    int pesato = atoi(argv[3]);			// 1 = vero 	0 = falso

    int n, arc, id;
    g>>n>>arc;

    cout<<n<<" "<<arc<<endl;

    tipo_inf* ospedale = new tipo_inf[n];

    cout<<"--- Database ospedale S.Maria Bianca ---"<<endl;
    for(int i = 1; i <= n; i++){
    	g.ignore();
    	g>>id;
    	g>>ospedale[i].cittadino_medico;
    	g>>ospedale[i].positivo_negativo;

    	cout<<id<<" "<<ospedale[i].cittadino_medico<<" "<<ospedale[i].positivo_negativo<<endl;

    }

    g.close();

    g.open("ospedale");
    graph G = graphBuild(g,orientato,pesato);

    cout<<"Il grafo ha dimensione: "<<get_dim(G)<<endl;
    cout<<endl;
    stampaGrafo(G);

    stampaRelazioni(G,ospedale);

    cout<<"Success!"<<endl;
}




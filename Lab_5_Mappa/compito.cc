#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;

#include "tipo.h"
#include "liste.h"
#include "grafo.h"

/*
 * acquisizione dei punti di interesse contenuti nel file PI.txt
 * nella lista ordinata pi e restituisce il numero di PI caricati;
 */
int carica(lista& pi){
	ifstream f("PI");
	int num = 0;

	tipo_inf temp;

	while(f.good()){
		f>>temp.id;
		f.ignore();
		f.getline(temp.nome,21);
		f>>temp.tipo;

		if(f.good() == false)
			break;
		else{
			elem* e = new_elem(temp);
			pi = ord_insert_elem(pi,e);
			num++;
		}
	}
	f.close();
	return num;
}

void stampa(lista pi){
	while(pi != NULL){
		print(head(pi));
		pi = tail(pi);
	}
}

/*
 * dato un identificativo, restituisce il corrispondente punto di interesse.
 * La lista è ordinata.
 */
tipo_inf search_pi(lista pi, int id){
	tipo_inf search;
	search.id = id;
	strcpy(search.nome, "");
	search.tipo = 'x';

	while(pi!=NULL && compare(pi->inf,search) <= 0){
		if(compare(pi->inf,search) == 0)
			return pi->inf;
		else
			pi = tail(pi);
	}
	search.id = 0;
	return search;
}

lista genera(lista pi, char tipo){
	lista g = NULL;

	while(pi != NULL){
		if(pi->inf.tipo == tipo){
			elem* e = new_elem(pi->inf);
			g = ord_insert_elem(g,e);
		}
		pi = tail(pi);
	}
	return g;
}

graph mappa(int n){
	ifstream f("G");
	graph g = new_graph(n);
	int u,v;
	int w = 1;

	do{
		f>>u;
		f>>v;
		add_edge(g,u,v,w);
	}while(f.good());

	return g;
}

void stampaMappa(graph G,lista pi){
	ofstream o("output");
	adj_list temp;
	for(int i = 1; i <= get_dim(G); i++){
		temp = get_adjlist(G,i);
		tipo_inf search = search_pi(pi,i);
		o<<search.nome;

		while(temp){
			tipo_inf adj = search_pi(pi,get_adjnode(temp));
			o<<"-->"<<adj.nome<<" ";
			temp = get_nextadj(temp);
		}
		o<<endl;
	}
	cout<<endl;
	cout<<endl;
	cout<<endl;

}

int main(){
	/* PUNTO 1 */
	int numPunti;
	lista puntiInteresse = NULL;
	numPunti = carica(puntiInteresse);

	cout<<"Il numero di punti di interesse è: "<<numPunti<<endl;

	stampa(puntiInteresse);
	cout<<endl;

	int id;
	do{
		cout<<"identificativo da cercare (press 0 to stop): ";
		cin>>id;

		tipo_inf s = search_pi(puntiInteresse, id);
		if(s.id == 0)
			cout<<"punto di interesse non presente"<<endl;
		else{
			cout<<"punto di interesse presente: ";
			print(s);
		}
	}while(id != 0);

	/* PUNTO 2B */
	lista g;
	char tipo;
	cout<<"Inserisci tipo (p - piazza, m - museo, e - edificio): ";
	cin>>tipo;
	g = genera(puntiInteresse,tipo);
	stampa(g);
	cout<<endl;

	/* PUNTO 2A */
	graph grafo;
	grafo = mappa(numPunti);
	cout<<"Il grafo ha dimensione: "<<grafo.dim<<endl;

	stampaMappa(grafo,puntiInteresse);

	cout<<"Success!"<<endl;
	return 0;
}

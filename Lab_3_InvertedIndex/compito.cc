#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;


#include "tipo.h"
#include "liste.h"
#include "parola.h"


/*
 * la funzione che carica l’inverted index dal file inverted.
 * Le parole con le relative informazioni devono essere memorizzate in un vettore dinamico
 * di tipo parola della dimensione corrispondente al numero di parole
 * (prima riga del file) che viene restituito dalla funzione stessa.
 */
parola* load(){
	int N;

	ifstream g("inverted");
	g>>N;

	parola* inverted = new parola[N];

	for(int i = 0; i < N; i++){
	    g.ignore();
	    g.getline(inverted[i].p, 80);
	    g>>inverted[i].n_doc;

	    inverted[i].l = NULL;     // inizializzo la lista a VUOTA
	    for(int j = 0; j < inverted[i].n_doc; j++){
	    	int doc = 0;
	        g>>doc;
	        elem* x = new_elem(doc);
	        inverted[i].l = insert_elem(inverted[i].l,x);
	    }
	}
	g.close();

	return inverted;
}

void stampa(parola* i, int N){
	cout<<"parola \t\t #doc \t\t docs"<<endl;
	for(int j = 0; j < N; j++){
		cout<<i[j].p<<"\t\t"<<i[j].n_doc<<"\t\t";
		lista l = i[j].l;

		while(l != NULL){
			cout<<head(l)<<" ";
			l = tail(l);
		}
		cout<<endl;
	}
}

/*
 * procedura che aggiorna l’inverted index caricando il contenuto del
 * documento contenuto nel file fileName (struttura di doc).
 *
 * Il codice deve gestire il caso di aggiunta di una parola,
 * di aggiunta di un id di documento alla posting list di una
 * parola già presente nell’inverted index.
 */
void update(parola* II, char* f, int &n){
    ifstream file(f);
    int id, i = 0;
    file>>id;

    while(file.good()){         // legge fino a quanto ci sono caratteri da leggere
        char word[20];
        file>>word;
        bool trovato = false;

        while(i < n && !trovato){
            if(strcmp(word, II[i].p) == 0){
                trovato = true;
                II[i].n_doc++;
                elem* e = new_elem(id);
                II[i].l = insert_elem(II[i].l,e);
            }
            i++;

        if(!trovato){
            n++;
            parola* temp = new parola[n];
            for(i = 0; i < n-1; i++){
                temp[i] = II[i];
            }

            II = new parola[n];
            for(i = 0; i < n-1; i++){
                II[i] = temp[i];
            }
            strcpy(II[i].p, word);
            II[i].n_doc = 1;
            II[i].l = NULL;
            elem* e = new_elem(id);
            II[i].l = insert_elem(II[i].l,e);
        }
        }
    file.close();
}
}


void AND(parola* II, char* W1, char* W2, int n){
    lista d1 = NULL;
    lista d2 = NULL;

    for(int i = 0; i < n; i++){
        if(strcmp(II[i].p, W1) == 0)
            d1 = II[i].l;
        if(strcmp(II[i].p, W2) == 0)
            d2 = II[i].l;
    }

    // intersezione tra due elementi- d1: 1 2 3 - d2: 2 4 - output: 2
    while(d2 != NULL){
        if(search(d1, head(d2)) != NULL)       // head(d2) è presente in d1
            cout<<head(d2)<<" ";
        d2 = tail(d2);
    }
}

int main(){

	/* PUNTO 1 */
	parola* i = load();

	int N;
	ifstream g;
	g.open("inverted");
	g>>N;
	stampa(i,N);

	/* PUNTO 2 */
	char* fileName = new char[20];
	cout<<"Nome del documento da aggiungere: ";
	cin>>fileName;
	update(i,fileName, N);
	stampa(i,N);

	/* PUNTO 3*/
	cout<<"Inserisci primo elemento: ";
	char W1[30];
	cin>>W1;
	cout<<"Inserisci secondo elemento: ";
	char W2[30];
	cin>>W2;
	AND(i,W1,W2,N);
	cout<<endl;


	cout<<"Success!"<<endl;
	return 0;
}


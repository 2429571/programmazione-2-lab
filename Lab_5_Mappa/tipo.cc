#include <iostream>
#include <cstring>
using namespace std;

#include "tipo.h"

int compare(tipo_inf a,tipo_inf b){
	return a.id-b.id;
}

void copy(tipo_inf& dest,tipo_inf source){
	dest = source;
}

void print(tipo_inf i){
	cout<<i.id<<" "<<i.nome<<" "<<i.tipo<<endl;
}



#include <iostream>
using namespace std;

#include "op.h"

int compare(tipo_inf i, tipo_inf j){
	return i.cifra-j.cifra;
}

void copy(tipo_inf& d, tipo_inf s){
	d.cifra = s.cifra;
	d.deb_cred = s.deb_cred;
	d.tipo = s.tipo;
}

void print(tipo_inf i){
	cout<<" "<<i.cifra<<" "<<i.deb_cred<<" "<<i.tipo<<endl;
}




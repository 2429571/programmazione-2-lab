#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;

#include "tipo.h"
#include "bst.h"
#include "persona.h"

persona* load(){
	ifstream f("popolazione");
	int N;
	f>>N;
	persona* p = new persona[N];			// vettore dinamico di persone con N = prima riga del file

	char* word = new char[5];				// vettore di caratteri per il codiceID della persona
	char* pos = new char[6];				// vettore di caratteri per il true o false altriementi si arrabbia
	int data,contagi;						// data e un intero che indica quanti contagiati ha effettuato una persona

	f.ignore();								// ignora lo spazio dopo la prima riga altriementi si arrabbia
	for(int i = 0; i < N; i++){				// per ogni persona
		if(i > 0) f.ignore();
		f.get(word, 5);						// prendo i dati dal file
		f.ignore();
		f.get(pos,6);
		f>>data;
		f>>contagi;

		strcpy(p[i].id,word);				// li metto nel vettore di tipo persona
		p[i].alberoContagi = NULL;

		if(strcmp(pos,"false") == 0){		// trasformo la il vettore di caratteri in booleano
			p[i].positivo = false;
			p[i].data = 0;
		}
		else{
			p[i].positivo = true;
			p[i].data = data;
		}

		if(p[i].positivo) cout<<p[i].id<<" è positivo (data "<<p[i].data<<") e ha contagiato "<<contagi<<" persone."<<endl;
		else cout<<p[i].id<<" è negativo (data "<<p[i].data<<") e ha contagiato "<<contagi<<" persone."<<endl;

		cout<<"I contagi di "<<p[i].id<<" sono: ";
		char* word1 = new char[5];			// vettore di caratteri per il codiceID del contagiato
		int data1;							// data di contagio del contagiato

		for(int j = 1; j <= contagi; j++){	// per ogni contagiato
			f.ignore();						// prendo i dati
			f.get(word1,5);
			f>>data1;

			cout<<word1<<" "<<data1<<" | ";	// stampa di verifica
			bnode* e = bst_newNode(data1, word1);		// costruisco il bst per ogni persona
			bst_insert(p[i].alberoContagi,e);
		}
		cout<<endl;
		cout<<endl;
	}

	f.close();

	return p;
}

void alberoDFS(bst b, ofstream& o){
	if(get_left(b) != NULL)
		alberoDFS(b->left,o);

	o<<b->inf<<" "<<b->key<<" | ";

	if(get_right(b) != NULL)
		alberoDFS(b->right,o);
}

void stampaAlbero(persona* p, int n){

	ofstream o("output");								// ho provato a mettere l'output in un file e funziona :)

	o<<"--- Sono la funzione stampa ---"<<endl;
	for(int i = 0; i < n; i++){							// per ogni persona
		o<<"I contagi di "<<p[i].id<<" sono: ";
		alberoDFS(p[i].alberoContagi,o);				// stampo l'albero di contagi
		o<<endl;
		o<<endl;
	}
}

int main(){

	/* PUNTO 1 */
	persona* immuni = load();

	ifstream f("popolazione");
	int n;
	f>>n;
	cout<<"Albero composto da "<<n<<" persone."<<endl;

	stampaAlbero(immuni,n);

	cout<<"Success!"<<endl;
	return 0;
}





